var gulp = require('gulp'),
	browserSync = require('browser-sync'),
	sass = require('gulp-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	autoprefixer = require('gulp-autoprefixer'),
	babel = require('gulp-babel'),
	useref = require('gulp-useref'),
	insert = require('gulp-insert'),
	gulpIf = require('gulp-if'),
  // To DO: add minification on deploy task
  // cleanCSS = require('gulp-clean-css'),
	concat = require('gulp-concat'),
	order = require('gulp-order'),
  through = require('through2'),
  cmq = require(__dirname + '/helpers/combineMedia.js'),
  injectPartials = require('gulp-inject-partials'),
  imagemin = require('gulp-imagemin'),
  changed = require('gulp-changed');

var tablet = '500px',
		desktop = '980px';

gulp.task('html', function(){
	return gulp.src('src/html/*.html')
		.pipe(useref())
    .pipe(injectPartials())
		.pipe(gulp.dest('dist'));
});

gulp.task('babel', function(){
	gulp.src('src/modules/**/*.js')
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(concat('script.js'))
        .pipe(gulp.dest('dist/js'))
        .pipe(browserSync.stream());
})

gulp.task('img', function () {
  return gulp.src('src/img/**/*.{jpg,jpeg,png,gif,svg}')
    .pipe(changed('dist/img'))
    .pipe(imagemin())
    .pipe(gulp.dest('dist/img'));
});

gulp.task('reload', function(){
	browserSync.reload();
});

gulp.task('serve', ['sass', 'html', 'babel'], function() {
	browserSync({
		server: 'dist'
	});

	gulp.watch('src/**/*.html', ['html', 'reload']);
	gulp.watch('src/**/*.scss', ['sass']);
	gulp.watch('src/**/*.js', ['babel']);
});

var isMedium = function(file) {
	if(file.history[0].indexOf('medium.scss') >= 0) {
		return true;
	}
	return false
}

var isWide = function(file) {
	if(file.history[0].indexOf('wide.scss') >= 0) {
		return true;
	}
	return false
}

gulp.task('learn', function(){
  console.log(gulp);
});

gulp.task('sass', function(){
	return gulp.src('src/**/*.scss')
		.pipe(sourcemaps.init())
		.pipe(gulpIf(isMedium, insert.prepend('@media screen and (min-width: '+tablet+') {')))
		.pipe(gulpIf(isMedium, insert.append('}')))
		.pipe(gulpIf(isWide, insert.prepend('@media screen and (min-width: '+desktop+') {')))
		.pipe(gulpIf(isWide, insert.append('}')))
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer({
			browsers: ['last 3 versions']
		}))
		.pipe(order([
			"global.scss",
			"medium.scss",
			"wide.scss"
		]))
		.pipe(concat('style.css'))
    .pipe(through.obj(function(chunk, enc, cb){
      chunk._contents = cmq.mergeMediaBlocks(chunk._contents, tablet);
      chunk._contents = cmq.mergeMediaBlocks(chunk._contents, desktop);
      this.push(chunk);
      cb();
    }))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('dist/css'))
		.pipe(browserSync.stream());
});

gulp.task('default', ['serve']);
