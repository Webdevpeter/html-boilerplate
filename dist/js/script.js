'use strict';

var timer = void 0;

function debounce(object, handler, time, eventType) {
  timer = setTimeout(function () {
    object.addEventListener(eventType, function listener(ev) {
      handler(ev);
      object.removeEventListener(eventType, listener);
      debounce(object, handler, time, eventType);
    });
  }, time);
}

function scrollHandler() {
  document.getElementById('mouse-pos').style.top = window.scrollY + 'px';
}
debounce(window, scrollHandler, 10, 'scroll');

window.addEventListener('resize', function () {
  return console.log('resize', window.outerWidth);
});