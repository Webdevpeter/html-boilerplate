var exports = module.exports = {};

exports.findBlockEnd = function (startingPoint, cc){
  i = cc.indexOf('{', startingPoint);
  var openCount = 0;
  var closeCount = 0;
  var endingPoint;
  while(true) {
    if(cc[i] === '{') {
      openCount++;
    }
    if(cc[i] === '}') {
      closeCount++;
    }
    if(openCount === closeCount) {
      endingPoint = i;
      break;
    }
    i++;
  }
  return endingPoint;
}

exports.mergeMediaBlocks = function (buffer, screenSize) {
  var newContent = buffer.toString();
  var mediaQueries = [];
  var mediaStringTemplate = '@media screen and (min-width: '+screenSize+') {';

  var firstMedia = newContent.indexOf(mediaStringTemplate);

  var i = 0;
  while(firstMedia >= 0) {
    mediaQueries.push(newContent.slice(firstMedia, this.findBlockEnd(firstMedia, Array.from(newContent)) + 1));
    newContent = newContent.replace(mediaQueries[i], '');
    firstMedia = newContent.indexOf(mediaStringTemplate);
    i++;
  }

  mediaQueries.forEach(function(el, index){
    el = el.replace(mediaStringTemplate, '');
    el = el.slice(0, -1);
    mediaQueries[index] = el;
  });

  var mergedQueries = mediaStringTemplate + mediaQueries.join('\n') + '}';

  newContent += mergedQueries;

  // change chars to ASCII
  var asciiContent = newContent.split('').map(function(itm){
      return itm.charCodeAt(0);
  });

  // console.log(newContent);
  return Buffer.from(asciiContent);
}
