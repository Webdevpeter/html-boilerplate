## Basic html boilerplate.

### Installation:

1. Download/clone repo.
    ```
    git clone https://gitlab.com/Webdevpeter/html-boilerplate.git
    ```
2. `cd html-boilerplate`
3. `npm install`
4. `gulp`


### Usage:

1. File structure.

   Development folder: `/src` --> `/dist`  
   HTML pages folder: `/src/html` --> `/dist`  
   Images folder: `/src/img` --> `/dist/img`  
   Modules folder: `/src/modules` --> `/dist`  
   Module css folder: `src/modules/{module_name}/css` --> `/dist/css/style.css`  
   Module html folder: `src/modules/{module_name}/html` --> ONLY INSERTABLE BY `<!-- partial -->`  
   Module js folder: `src/modules/{module_name}/js` --> `/dist/js/script.js`  


2. Inserting modules to HTML pages:

```
<!-- partial:../modules/{module_name}/html/{file_name}.html -->
<!-- partial -->
```

3. Tasks:

   a) `gulp` - serve on port :3000  
   b) `gulp img` - minify images

   NOTE: Always use `gulp img` after placing new files into `/src/img`.


4. To refer images in scss files use path `../img/{name_of_the_file}`.
